# Fravega test

Este es un proyecto solicitado por la compañia para medir habilidades de testing.
## Comenzando 🚀

Uso tanto Behaviour Driven Development (BDD) como unittest para los asserts.

### Pre-requisitos 📋

Necesitas Python 3 junto con Selenium y Behave para correr los scripts

### Instalación 🔧

Para hacer la instalación en windows, descargamos Python3 y el webdriver de Chrome.
```
https://www.python.org/downloads/

https://sites.google.com/a/chromium.org/chromedriver/downloads

```
Instalamos Python 3 y al webdriver lo descomprimimos y copiamos al inicio del disco C (C:\chromedriver.exe)

Luego...

Desde la consola (CMD) instalamos Selenium, Behave, Requests y HTMLTestRunner (Ambas versiones para asegurarnos funcione)
```
pip3 install selenium
pip3 install behave
pip3 install requests
pip3 install html-testRunner
pip3 install HTMLTestRunner-Python3
```

Si confirma que todo se descargó e instaló correctamente, estamos listos para hacer las pruebas

## Ejecutando las pruebas ⚙️
### Evalue las pruebas del backend 🔩
Para ejecutar las pruebas debemos posicionarnos en la carpeta de Fravega y desde allí ejecutamos en la consola (CMD):

```
Python backend.py
```
y podemos ver en acción las libreria de Requests con unittest, una vez finalizado el testing se creará una carpeta llamada 'backend', ingresamos y abrimos el archivo html.


### Y las pruebas del Front-end ⌨️

Para el frontend usamos el framework de Behave con Gherkins, para ello debemos posicionarnos en la carpeta de Fravega y desde allí ejecutamos en la consola (CMD):

```
behave
```

y veremos en acción a Selenium y desde la consola un testing usando Gherkins.

## Licencia 📄

Al hacer uso de este software y comprobar que funciona correctamente, la empresa se obliga a hacer una oferta laboral que resulte 'irresistible'.

## Expresiones de Gratitud 🎁

* Agradecido por la oportunidad y exitos mejorando cada día el servicio al cliente.
