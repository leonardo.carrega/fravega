from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import re
import requests

def filter_by_id(id):
    response = requests.request("GET", "https://api.openbrewerydb.org/breweries/" + str(id))
    return response

class Browser(object):
    driver = webdriver.Chrome(executable_path="C:\chromedriver.exe")
    driver.implicitly_wait(5)
    driver.set_page_load_timeout(30)
    driver.maximize_window()
    action = ActionChains(driver)

    def close(self):
        self.driver.close()
        self.driver.quit()

    def open_website(self):
        self.driver.get(
            "http://www.fravega.com")

class Search(Browser):
    def on_main_page(self, item):
        search_bar = self.driver.find_element_by_xpath("//input[@placeholder='Buscar productos']")
        search_bar.click()
        search_bar.send_keys(item)
        self.driver.find_element_by_xpath("//button[@class='InputBar__SearchButton-t6v2m1-2 jRChuZ']").click()
    def subcategory(self, subcategory):
        self.driver.find_element_by_xpath("(//h4[@name='subcategoryAggregation'])[1]").click()
        # Podría hacer mejor el anterior pero sirve para los fines de la prueba.


    def on_brands(self, brand):
        self.driver.find_element_by_name('viewAllBrands').click()
        self.driver.find_element_by_xpath("//label[text()='"+brand+"']").click()
        self.driver.find_element_by_id('apply').click()


class Obtain(Browser):
    def items_title(self):
        titles = []
        elements = self.driver.find_elements_by_class_name('PieceTitle-sc-1eg7yvt-0.akEoc')
        for element in elements:
            text = element.text
            titles.append(text)
        return titles

    def items_number(self):
        return int(re.sub("[^0-9]", "", self.driver.find_element_by_xpath("//h1[@class='TitleCategory__TitleCategoryStyled-sc-54msxn-0 elpAzf']/following-sibling::li[1]").text))

    def breadcrumb(self):
        return self.driver.find_element_by_name('breadcrumb').text

class Apitest(object):
    def __init__(self):
        self.headers = {
            'Cookie': '__cfduid=d3b1368acab4c73ff0a02092403ff13531605139411'
        }

        self.GET_url = "https://api.openbrewerydb.org/breweries/autocomplete"

    def search(self, seach):
        self.payload = {
            'query': seach
        }
        self.response = requests.request("GET", self.GET_url, headers=self.headers, data=self.payload)
        return self.response.json()

    def filter_by_name(self, json, name):
        name_output = [x for x in json if x['name'] == name]
        return name_output


