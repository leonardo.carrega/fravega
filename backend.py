import unittest
import HtmlTestRunner
import requests

headers = {
    'Cookie': '__cfduid=d3b1368acab4c73ff0a02092403ff13531605139411'
}


payload = {
    'query': '"lagunitas"'
}

GET_url = "https://api.openbrewerydb.org/breweries/autocomplete"

GET_by_url = "https://api.openbrewerydb.org/breweries/"


# Obtener una lista de cervecerías que contengan el texto "lagunitas" en su nombre.
# Para ello, se debe ejecutar el siguiente servicio, indicando el texto a buscar en el queryParam "query".
response = requests.request("GET", GET_url, headers=headers, data=payload)

print(response.json())


# De la lista de resultados del punto 1, tomar aquellos que contengan en la key "name", el valor "Lagunitas Brewing Co".
name_output = [x for x in response.json() if x['name'] == "Lagunitas Brewing Co"]

# print(name_output[1]['id'])
print('Key name = "Lagunitas Brewing Co"')
print(name_output)

#  través del siguiente servicio, obtener el detalle de cada cervecería de la lista del punto 2 y tomar solo el que contenga
# "state" = "California".
for output in name_output:
    by_id = requests.request("GET", GET_by_url+output['id'], headers=headers, data=payload)
    if by_id.json()['state'] == "California":
        to_test = by_id.json()

print('"state" = "California"')
print(to_test)

print(to_test['id'])

print(to_test['name'])

print(to_test['street'])

print(to_test['phone'])


class my_unittest(unittest.TestCase):
    def setUp(self):
        pass

    def test_id(self):
        assert(to_test['id'] == 761)

    def test_name(self):
        assert(to_test['name'] == "Lagunitas Brewing Co")

    def test_street(self):
        assert(to_test['street'] == "1280 N McDowell Blvd")

    def test_phone(self):
        assert(to_test['phone'] == "7077694495")




    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='backend'))
