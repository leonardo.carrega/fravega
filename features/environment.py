from POM import *

def before_all(context):
    context.driver = Browser()  # Main browser configuration
    context.search = Search()
    context.obtain = Obtain()
    print('Environment works')
    context.driver.open_website()

def after_all(context):
    context.driver.close()