from behave import *

@given("I am searching for {item}")
def step_impl(context, item):
    context.search.on_main_page(item)

@when("I select {item} as a subcategory")
def step_impl(context, item):
    context.search.subcategory(item)


@step("select {trademark} as trademark")
def step_impl(context, trademark):
    context.search.on_brands(trademark)

@then("every item title must have {trademark}")
def step_impl(context, trademark):
    titles = context.obtain.items_title()
    for title in titles:
        try:
            assert(trademark in title)  # Verificar la marca esté contenida en el texto.
        except:
            print(trademark+' no se encuenta en todos los titulos')
        finally:
            continue

@step("elements must be the same than in the list")
def step_impl(context):
    # Comparo la cantidad de resultados del front, con la cantidad de titulos que tengo
    try:
        assert(context.obtain.items_number() == len(context.obtain.items_title()))
    except:
        print('No hay la misma cantidad de elementos que los indicados')

@step("the breadcrumb must be '{breadcrumb}'")
def step_impl(context, breadcrumb):
    try:
        assert (breadcrumb in context.obtain.breadcrumb())
    except:
        print('El breadcrumb no es igual a: '+breadcrumb )


