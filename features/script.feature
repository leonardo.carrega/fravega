Feature: Automate Fravega

	# Cambiando los valores se puede cambiar las variables a probar
	Scenario: Search a Samsung fridge
		Given I am searching for Heladera
		When I select Heladeras as a subcategory
		And select Columbia as trademark
		Then every item title must have Columbia
		And elements must be the same than in the list
		And the breadcrumb must be 'Heladeras con Freezer'

	# Con un scenario outline se podría crear una tabla para hacer varías pruebas con poco código.